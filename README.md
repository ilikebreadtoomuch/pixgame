# PixGame

A recreation of older versions of Doukutsu Monogatari using decompilation code and development logs.

## Inaccuracies

This is a list of major inaccuracies to actual old builds of Doukutsu Monogatari. 

The following list of inaccuracies will (most likely) not be fixed for compatibility's sake:

- The game has been translated to English. The actual beta was not in English.
- Everything before Pix0250\* did not have Config.cpp implemented.
- The game always started in fullscreen. Because of this the game also most likely did not have "Quit" and "About" menus.

The following list of inaccuracies will be fixed on further information:

- The executable icons before Pix0208\* are unknown.
- Details about player movement before Pix0204 is unknown.
- Pix0001-Pix0003's functionality is unknown. Assumed to be simple map viewers.
- The tile placement of the room `Green` is mostly made up.

*\* Not completely sure*.

## Building

This project uses CMake.

Switch to the terminal (Visual Studio users should open the [Developer Command Prompt](https://docs.microsoft.com/en-us/dotnet/framework/tools/developer-command-prompt-for-vs))
and `cd` into this folder. After that, generate the files for your build system
with:

```
cmake -B build -DCMAKE_BUILD_TYPE=Release
```

MSYS2 users should append `-G"MSYS Makefiles"` to this command, also.

You can also add the following flags:

Name | Function
--------|--------
`-DFIX_BUGS=ON` | Fix various bugs in the game
`-DFIX_MAJOR_BUGS=ON` | Fix bugs that invoke undefined behavior or cause memory leaks 
`-DLTO=ON` | Enable link-time optimization 
`-DMSVC_LINK_STATIC_RUNTIME=ON` | Link the static MSVC runtime library, to reduce the number of required DLL files (Visual Studio only)

You can pass your own compiler flags with `-DCMAKE_C_FLAGS` and `-DCMAKE_CXX_FLAGS`.

You can then compile the game with this command:

```
cmake --build build --config Release
```

If you're a Visual Studio user, you can open the generated `Doukutsu.sln` file
instead, which can be found in the `build` folder.

Once built, the executable can be found in the `game` folder.

## Licensing

Decompilation code and assets owned by Nicalis Inc., Clownacy, CuckyDev and GabrielTFS.

This repo's original assets and/or code is under the following license:

```
MIT License

Copyright (c) 2021 Yasin, Wirelex.exe

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
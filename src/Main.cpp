#include "Main.h"

#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include <shlwapi.h>

#include "WindowsWrapper.h"

#include "CommonDefines.h"
#include "Config.h"
#include "Dialog.h"
#include "Draw.h"
#include "Game.h"
#include "Generic.h"
#include "KeyControl.h"

LRESULT CALLBACK WindowProcedure(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

char gModulePath[MAX_PATH];
char gDataPath[MAX_PATH];

HWND ghWnd;
BOOL bFullscreen;

static BOOL bActive = TRUE;

static HANDLE hObject;
static HANDLE hMutex;
static HINSTANCE ghInstance;

static int windowWidth;
static int windowHeight;

static const char* const mutex_name = "Doukutsu";

static const char* const lpWindowName = "Pix0201";

// The original name for this function is unknown
void SetWindowName(HWND hWnd)
{
	char window_name[0x100];

	sprintf(window_name, "%s", lpWindowName);
	SetWindowTextA(hWnd, window_name);
}

// TODO - Inaccurate stack frame
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	(void)hPrevInstance;
	(void)lpCmdLine;
	(void)nShowCmd;

	hObject = OpenMutexA(MUTEX_ALL_ACCESS, 0, mutex_name);
	if (hObject != NULL)
	{
		CloseHandle(hObject);
		return 0;
	}

	hMutex = CreateMutexA(NULL, FALSE, mutex_name);

	ghInstance = hInstance;

	// Get executable's path
	GetModuleFileNameA(NULL, gModulePath, MAX_PATH);
	PathRemoveFileSpecA(gModulePath);

	// Get path of the data folder
	strcpy(gDataPath, gModulePath);
	strcat(gDataPath, "\\data");

	CONFIGDATA conf;
	if (!LoadConfigData(&conf))
		DefaultConfigData(&conf);

	// Apply keybinds
	// Swap X and Z buttons
	switch (conf.attack_button_mode)
	{
		case 0:
			gKeyJump = KEY_Z;
			gKeyShot = KEY_X;
			break;

		case 1:
			gKeyJump = KEY_X;
			gKeyShot = KEY_Z;
			break;
	}

	// Swap Okay and Cancel buttons
	switch (conf.ok_button_mode)
	{
		case 0:
			gKeyOk = gKeyJump;
			gKeyCancel = gKeyShot;
			break;

		case 1:
			gKeyOk = gKeyShot;
			gKeyCancel = gKeyJump;
			break;
	}

	// Swap left and right weapon switch keys
	if (IsKeyFile("s_reverse"))
	{
		gKeyArms = KEY_ARMSREV;
		gKeyArmsRev = KEY_ARMS;
	}

	// Alternate movement keys
	switch (conf.move_button_mode)
	{
		case 0:
			gKeyLeft = KEY_LEFT;
			gKeyUp = KEY_UP;
			gKeyRight = KEY_RIGHT;
			gKeyDown = KEY_DOWN;
			break;

		case 1:
			gKeyLeft = KEY_ALT_LEFT;
			gKeyUp = KEY_ALT_UP;
			gKeyRight = KEY_ALT_RIGHT;
			gKeyDown = KEY_ALT_DOWN;
			break;
	}

	RECT unused_rect = {0, 0, WINDOW_WIDTH, WINDOW_HEIGHT};

	WNDCLASSEXA wndclassex;
	memset(&wndclassex, 0, sizeof(WNDCLASSEXA));
	wndclassex.cbSize = sizeof(WNDCLASSEXA);
	wndclassex.lpfnWndProc = WindowProcedure;
	wndclassex.hInstance = hInstance;
	wndclassex.hbrBackground = (HBRUSH)GetStockObject(DKGRAY_BRUSH);	// This is what gives the window's undrawn regions its grey colour
	wndclassex.lpszClassName = lpWindowName;
	wndclassex.hCursor = LoadCursorA(hInstance, "CURSOR_NORMAL");

	HWND hWnd;
	HMENU hMenu;
	static DWORD gangnamStyle = (WS_MINIMIZEBOX | WS_SYSMENU | WS_BORDER | WS_DLGFRAME | WS_VISIBLE), gangnamStyleNinja = (WS_EX_LEFT | WS_EX_LTRREADING | WS_EX_RIGHTSCROLLBAR);
	RECT windowRect;
	int nWidth;
	int nHeight;
	int x;
	int y;

	switch (conf.display_mode)
	{
		case 1:
		case 2:
			wndclassex.lpszMenuName = "MENU_MAIN";

			if (RegisterClassExA(&wndclassex) == 0)
			{
				ReleaseMutex(hMutex);
				return 0;
			}

			// Set window dimensions
			if (conf.display_mode == 1)
			{
				windowWidth = WINDOW_WIDTH;
				windowHeight = WINDOW_HEIGHT;
			}
			else
			{
				windowWidth = WINDOW_WIDTH * 2;
				windowHeight = WINDOW_HEIGHT * 2;
			}

			windowRect.left = 0;
			windowRect.top = 0;
			windowRect.right = windowWidth;
			windowRect.bottom = windowHeight;

			AdjustWindowRectEx(&windowRect, gangnamStyle, TRUE, gangnamStyleNinja);

			nWidth = windowRect.right - windowRect.left;
			nHeight = windowRect.bottom - windowRect.top;

			x = (GetSystemMetrics(SM_CXSCREEN) - nWidth) / 2;
			y = (GetSystemMetrics(SM_CYSCREEN) - nHeight) / 2;

			SetClientOffset(GetSystemMetrics(SM_CXFIXEDFRAME) + GetSystemMetrics(SM_CXPADDEDBORDER) + (GetSystemMetrics(SM_CXPADDEDBORDER) == 0 ? 0 : 1), GetSystemMetrics(SM_CYFIXEDFRAME) + GetSystemMetrics(SM_CYCAPTION) + GetSystemMetrics(SM_CYMENU) + GetSystemMetrics(SM_CXPADDEDBORDER) + (GetSystemMetrics(SM_CXPADDEDBORDER) == 0 ? 0 : 1));

			hWnd = CreateWindowExA(gangnamStyleNinja, lpWindowName, lpWindowName, gangnamStyle, x, y, nWidth, nHeight, NULL, NULL, hInstance, NULL);
			ghWnd = hWnd;

			if (hWnd == NULL)
			{
				ReleaseMutex(hMutex);
				return 0;
			}

			hMenu = GetMenu(hWnd);

		#ifdef FIX_MAJOR_BUGS
			if (conf.display_mode == 1)
			{
				if (!StartDirectDraw(hWnd, 0, 0))
				{
					ReleaseMutex(hMutex);
					return 0;
				}
			}
			else
			{
				if (!StartDirectDraw(hWnd, 1, 0))
				{
					ReleaseMutex(hMutex);
					return 0;
				}
			}
		#else
			// Doesn't handle StartDirectDraw failing
			if (conf.display_mode == 1)
				StartDirectDraw(hWnd, 0, 0);
			else
				StartDirectDraw(hWnd, 1, 0);
		#endif

			break;

		case 0:
		case 3:
		case 4:
			if (RegisterClassExA(&wndclassex) == 0)
			{
				ReleaseMutex(hMutex);
				return 0;
			}

			// Set window dimensions
			windowWidth = WINDOW_WIDTH;
			windowHeight = WINDOW_HEIGHT;

			SetClientOffset(0, 0);

			hWnd = CreateWindowExA(WS_EX_LEFT | WS_EX_LTRREADING | WS_EX_RIGHTSCROLLBAR, lpWindowName, lpWindowName, WS_SYSMENU | WS_VISIBLE | WS_POPUP, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), NULL, NULL, hInstance, NULL);
			ghWnd = hWnd;

			if (hWnd == NULL)
			{
				ReleaseMutex(hMutex);
				return 0;
			}

			// Set colour depth
			int depth;

			switch (conf.display_mode)
			{
				case 0:
					depth = 16;
					break;
				case 3:
					depth = 24;
					break;
				case 4:
					depth = 32;
					break;
			}

		#ifdef FIX_MAJOR_BUGS
			if (!StartDirectDraw(hWnd, 2, depth))
			{
				ReleaseMutex(hMutex);
				return 0;
			}
		#else
			// Doesn't handle StartDirectDraw failing
			StartDirectDraw(hWnd, 2, depth);
		#endif

			bFullscreen = TRUE;

			ShowCursor(FALSE);
			break;
	}

	// Set rects
	RECT rcFull = {0, 0, 0, 0};
	rcFull.right = WINDOW_WIDTH;
	rcFull.bottom = WINDOW_HEIGHT;

	// Draw loading screen
	CortBox(&rcFull, 0x000000);

	// Draw to screen
	if (!Flip_SystemTask(ghWnd))
	{
		ReleaseMutex(hMutex);
		return 1;
	}

	InitTextObject(conf.font_name);

	// Run game code
	Game(hWnd);

	// End stuff
	EndTextObject();
	EndDirectDraw(hWnd);

	ReleaseMutex(hMutex);

	return 1;
}

void InactiveWindow(void)
{
	if (bActive)
		bActive = FALSE;
}

void ActiveWindow(void)
{
	if (!bActive)
		bActive = TRUE;
}

// TODO - Inaccurate stack frame
LRESULT CALLBACK WindowProcedure(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	BOOL window_focus;

	switch (Msg)
	{
		case WM_CREATE:
			DrawMenuBar(hWnd);

			if (!bFullscreen)
				LoadWindowRect(hWnd, "window.rect", FALSE);

			SetWindowName(hWnd);

			break;

		case WM_SYSCOMMAND:
			switch (wParam)
			{
				case SC_MONITORPOWER:
					break;

				case SC_KEYMENU:
					break;

				case SC_SCREENSAVE:
					break;

				default:
					DefWindowProcA(hWnd, Msg, wParam, lParam);
					break;
			}

			break;

		case WM_IME_NOTIFY:
			if (wParam == IMN_SETOPENSTATUS)
			{
				HIMC hImc = ImmGetContext(hWnd);
				ImmSetOpenStatus(hImc, 0);
				ImmReleaseContext(hWnd, hImc);
			}

			break;

		case WM_KEYDOWN:
			switch (wParam)
			{
				case VK_ESCAPE:
					gKey |= KEY_ESCAPE;
					break;

				case 'W':
					gKey |= KEY_MAP;
					break;

				case VK_LEFT:
					gKey |= KEY_LEFT;
					break;

				case VK_RIGHT:
					gKey |= KEY_RIGHT;
					break;

				case VK_UP:
					gKey |= KEY_UP;
					break;

				case VK_DOWN:
					gKey |= KEY_DOWN;
					break;

				case 'X':
					gKey |= KEY_X;
					break;

				case 'Z':
					gKey |= KEY_Z;
					break;

				case 'S':
					gKey |= KEY_ARMS;
					break;

				case 'A':
					gKey |= KEY_ARMSREV;
					break;

				case VK_SHIFT:
					gKey |= KEY_SHIFT;
					break;

				case VK_F1:
					gKey |= KEY_F1;
					break;

				case VK_F2:
					gKey |= KEY_F2;
					break;

				case 'Q':
					gKey |= KEY_ITEM;
					break;

				case VK_OEM_COMMA:
					gKey |= KEY_ALT_LEFT;
					break;

				case VK_OEM_PERIOD:
					gKey |= KEY_ALT_DOWN;
					break;

				case VK_OEM_2:
					gKey |= KEY_ALT_RIGHT;
					break;

				case 'L':
					gKey |= KEY_L;
					break;

				case VK_OEM_PLUS:
					gKey |= KEY_PLUS;
					break;
			}

			break;

		case WM_KEYUP:
			switch (wParam)
			{
				case VK_ESCAPE:
					gKey &= ~KEY_ESCAPE;
					break;

				case 'W':
					gKey &= ~KEY_MAP;
					break;

				case VK_LEFT:
					gKey &= ~KEY_LEFT;
					break;

				case VK_RIGHT:
					gKey &= ~KEY_RIGHT;
					break;

				case VK_UP:
					gKey &= ~KEY_UP;
					break;

				case VK_DOWN:
					gKey &= ~KEY_DOWN;
					break;

				case 'X':
					gKey &= ~KEY_X;
					break;

				case 'Z':
					gKey &= ~KEY_Z;
					break;

				case 'S':
					gKey &= ~KEY_ARMS;
					break;

				case 'A':
					gKey &= ~KEY_ARMSREV;
					break;

				case VK_SHIFT:
					gKey &= ~KEY_SHIFT;
					break;

				case VK_F1:
					gKey &= ~KEY_F1;
					break;

				case VK_F2:
					gKey &= ~KEY_F2;
					break;

				case 'Q':
					gKey &= ~KEY_ITEM;
					break;

				case VK_OEM_COMMA:
					gKey &= ~KEY_ALT_LEFT;
					break;

				case VK_OEM_PERIOD:
					gKey &= ~KEY_ALT_DOWN;
					break;

				case VK_OEM_2:
					gKey &= ~KEY_ALT_RIGHT;
					break;

				case 'L':
					gKey &= ~KEY_L;
					break;

				case VK_OEM_PLUS:
					gKey &= ~KEY_PLUS;
					break;
			}

			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case 40001:
					if (DialogBoxParamA(ghInstance, "DLG_YESNO", hWnd, QuitDialog, (LPARAM)"Quit?") == 1)
						PostMessageA(hWnd, WM_CLOSE, 0, 0);
					break;

				case 40002:
					DialogBoxParamA(ghInstance, "DLG_ABOUT", hWnd, VersionDialog, 0);
					break;
			}

			break;

		case WM_ACTIVATE:
			switch (LOWORD(wParam))
			{
				case WA_INACTIVE:
					window_focus = FALSE;
					break;

				case WA_ACTIVE:
				case WA_CLICKACTIVE:
					if (HIWORD(wParam) != 0)
						window_focus = FALSE;
					else
						window_focus = TRUE;

					break;
			}

			if (window_focus)
				ActiveWindow();
			else
				InactiveWindow();

			break;

		case WM_CLOSE:
			PostQuitMessage(0);
			break;

		default:
			return DefWindowProcA(hWnd, Msg, wParam, lParam);
	}

	return 1;
}

BOOL SystemTask(void)
{
	MSG Msg;

	while (PeekMessageA(&Msg, NULL, 0, 0, PM_NOREMOVE) || !bActive)
	{
		if (!GetMessageA(&Msg, NULL, 0, 0))
			return FALSE;

		TranslateMessage(&Msg);
		DispatchMessageA(&Msg);
	}

	return TRUE;
}

#include "Stage.h"

#include <stdio.h>
#include <string.h>

#include "WindowsWrapper.h"

#include "Draw.h"
#include "Frame.h"
#include "Map.h"
#include "MyChar.h"
#include "TextScr.h"

int gStageNo;
unsigned int gOldPos;

// Note: Pixel made numerous capitalisation errors when creating this table.
// This isn't a problem for Windows, because of its case-insensitive filesystem.
const STAGE_TABLE gTMT[] = {
	{"0", "0"},
	{"Parts", "Green"}
};

BOOL TransferStage(int no, int w, int x, int y)
{
	char path[MAX_PATH];
	char path_dir[20];
	BOOL bError;

	// Move character
	SetMyCharPosition(x * 0x10 * 0x200, y * 0x10 * 0x200);

	bError = FALSE;

	// Get path
	strcpy(path_dir, "Stage");

	// Load tileset
	sprintf(path, "%s\\Prt%s", path_dir, gTMT[no].parts);
	if (!ReloadBitmap_File(path, SURFACE_ID_LEVEL_TILESET))
		bError = TRUE;

	sprintf(path, "%s\\%s.pxa", path_dir, gTMT[no].parts);
	if (!LoadAttributeData(path))
		bError = TRUE;

	// Load tilemap
	sprintf(path, "%s\\%s.pxm", path_dir, gTMT[no].map);
	if (!LoadMapData2(path))
		bError = TRUE;

	// Load script
	sprintf(path, "%s\\%s.tsc", path_dir, gTMT[no].map);
	if (!LoadTextScript2(path))
		bError = TRUE;

	if (bError)
		return FALSE;

	StartTextScript(w);
	SetFrameMyChar();
	gStageNo = no;

	return TRUE;
}

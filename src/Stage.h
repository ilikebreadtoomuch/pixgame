#pragma once

#include "WindowsWrapper.h"

typedef struct STAGE_TABLE
{
	char parts[0x20];
	char map[0x20];
} STAGE_TABLE;

extern int gStageNo;
extern unsigned int gOldPos;

extern const STAGE_TABLE gTMT[2];

BOOL TransferStage(int no, int w, int x, int y);

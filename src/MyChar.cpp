// THIS IS DECOMPILED PROPRIETARY CODE - USE AT YOUR OWN RISK.
//
// The original code belongs to Daisuke "Pixel" Amaya.
//
// Modifications and custom code are under the MIT licence.
// See LICENCE.txt for details.

#include "MyChar.h"

#include <stddef.h>
#include <string.h>

#include "WindowsWrapper.h"

#include "CommonDefines.h"
#include "Draw.h"
#include "Game.h"
#include "KeyControl.h"


MYCHAR gMC;

void InitMyChar(void)
{
	memset(&gMC, 0, sizeof(MYCHAR));

	gMC.rect = {0, 0, 16, 16};

	gMC.view.back = 8 * 0x200;
	gMC.view.top = 8 * 0x200;
	gMC.view.front = 8 * 0x200;
	gMC.view.bottom = 8 * 0x200;
}

void ActMyChar(void)
{
	if (gKey & (gKeyLeft | gKeyRight))
	{
		if (gKey & gKeyLeft)
			gMC.xm -= 0x100;

		if (gKey & gKeyRight)
			gMC.xm += 0x100;
	}
	else if (gMC.xm < 0x80 && gMC.xm > -0x80)
	{
		gMC.xm = 0;
	}
	else if (gMC.xm > 0)
	{
		gMC.xm -= 0x80;
	}
	else if (gMC.xm < 0)
	{
		gMC.xm += 0x80;
	}

	if (gKey & (gKeyUp | gKeyDown))
	{
		if (gKey & gKeyUp)
			gMC.ym -= 0x100;

		if (gKey & gKeyDown)
			gMC.ym += 0x100;
	}
	else if (gMC.ym < 0x80 && gMC.ym > -0x80)
	{
		gMC.ym = 0;
	}
	else if (gMC.ym > 0)
	{
		gMC.ym -= 0x80;
	}
	else if (gMC.ym < 0)
	{
		gMC.ym += 0x80;
	}

	if (gMC.xm > 0x400)
		gMC.xm = 0x400;
	if (gMC.xm < -0x400)
		gMC.xm = -0x400;

	if (gMC.ym > 0x400)
		gMC.ym = 0x400;
	if (gMC.ym < -0x400)
		gMC.ym = -0x400;

	if ((gKey & (gKeyLeft | gKeyUp)) == (gKeyLeft | gKeyUp))
	{
		if (gMC.xm < -780)
			gMC.xm = -780;
		if (gMC.ym < -780)
			gMC.ym = -780;
	}

	if ((gKey & (gKeyRight | gKeyUp)) == (gKeyRight | gKeyUp))
	{
		if (gMC.xm > 780)
			gMC.xm = 780;
		if (gMC.ym < -780)
			gMC.ym = -780;
	}

	if ((gKey & (gKeyLeft | gKeyDown)) == (gKeyLeft | gKeyDown))
	{
		if (gMC.xm < -780)
			gMC.xm = -780;
		if (gMC.ym > 780)
			gMC.ym = 780;
	}

	if ((gKey & (gKeyRight | gKeyDown)) == (gKeyRight | gKeyDown))
	{
		if (gMC.xm > 780)
			gMC.xm = 780;
		if (gMC.ym > 780)
			gMC.ym = 780;
	}

	// Camera
	if (gMC.index_y > 0x200)
		gMC.index_y -= 0x200;
	if (gMC.index_y < -0x200)
		gMC.index_y += 0x200;

	gMC.tgt_x = gMC.x + gMC.index_x;
	gMC.tgt_y = gMC.y + gMC.index_y;

	gMC.x += gMC.xm;
	gMC.y += gMC.ym;
}

void GetMyCharPosition(int *x, int *y)
{
	*x = gMC.x;
	*y = gMC.y;
}

void SetMyCharPosition(int x, int y)
{
	gMC.x = x;
	gMC.y = y;
	gMC.tgt_x = gMC.x;
	gMC.tgt_y = gMC.y;
	gMC.index_x = 0;
	gMC.index_y = 0;
	gMC.xm = 0;
	gMC.ym = 0;
}

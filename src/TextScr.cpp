#include "TextScr.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "WindowsWrapper.h"

#include "CommonDefines.h"
#include "Draw.h"
#include "Game.h"
#include "Generic.h"
#include "KeyControl.h"
#include "Main.h"
#include "Map.h"
#include "Stage.h"

// This limits the size of a .tsc script to 0x5000 bytes (the game will crash above this)
#define TSC_BUFFER_SIZE 0x5000

#define TEXT_LEFT (WINDOW_WIDTH / 2 - 108)

#define IS_COMMAND(c1, c2, c3) (gTS.data[gTS.p_read + 1] == (c1) && gTS.data[gTS.p_read + 2] == (c2) && gTS.data[gTS.p_read + 3] == (c3))

// Custom
#define LINES 4
#define TEXT_SHADOW RGB(0x24, 0x6C, 0x7C)
#define TEXT_COLOR RGB(0xFF, 0xFF, 0xFE)

TEXT_SCRIPT gTS;

static char text[LINES + 1][0x40];

const RECT gRect_line = {0, 0, 216, 16};

#ifdef FIX_BUGS
static unsigned long nod_color;
#endif

// Initialize and end tsc
BOOL InitTextScript2(void)
{
	int i;

	// Clear flags
	gTS.mode = 0;
	g_GameFlags &= ~4;

	// Create line surfaces
	for (i = 0; i < LINES + 1; ++i)
		MakeSurface_Generic(gRect_line.right, gRect_line.bottom, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + i), FALSE);

	// Clear text
	memset(text, 0, sizeof(text));

	// Allocate script buffer
	gTS.data = (char*)malloc(TSC_BUFFER_SIZE);

	if (gTS.data == NULL)
		return FALSE;
	else
		return TRUE;
}

void EndTextScript(void)
{
	int i;

	// Free TSC buffer
	free(gTS.data);

	// Release buffers
	ReleaseSurface(SURFACE_ID_TEXT_BOX);

	for (i = 0; i < LINES + 1; ++i)
		ReleaseSurface((SurfaceID)(SURFACE_ID_TEXT_LINE1 + i));
}

// Decrypt .tsc
void EncryptionBinaryData2(unsigned char *pData, long size)
{
	int i;
	int work;

	int half;
	int val1;

	half = size / 2;

	if (pData[half] == 0)
		val1 = -7;
	else
		val1 = (pData[half] % 0x100) * -1;

	for (i = 0; i < size; ++i)
	{
		work = pData[i];
		work += val1;

		if (i != half)
			pData[i] = work % 0x100;
	}
}

// Load generic .tsc
BOOL LoadTextScript2(const char *name)
{
	FILE *fp;
	char path[MAX_PATH];

	// Get path
	sprintf(path, "%s\\%s", gDataPath, name);

	gTS.size = GetFileSizeLong(path);
	if (gTS.size == INVALID_FILE_SIZE)
		return FALSE;

	// Open file
	fp = fopen(path, "rb");
	if (fp == NULL)
		return FALSE;

	// Read data. Note that gTS.size may exceed the size of 'gTS.data' (TSC_BUFFER_SIZE)
	fread(gTS.data, 1, gTS.size, fp);
	gTS.data[gTS.size] = '\0';
	fclose(fp);

	// Set path
	strcpy(gTS.path, name);

	// Decrypt data
	EncryptionBinaryData2((unsigned char*)gTS.data, gTS.size);

	return TRUE;
}

// Get current path
void GetTextScriptPath(char *path)
{
	strcpy(path, gTS.path);
}

int GetTextScriptNo(int a)
{
	int b = 0;
	b += (gTS.data[a++] - '0') * 1000;
	b += (gTS.data[a++] - '0') * 100;
	b += (gTS.data[a++] - '0') * 10;
	b += gTS.data[a] - '0';
	return b;
}

// Start TSC event
BOOL StartTextScript(int no)
{
	int i;

	// Reset state
	gTS.mode = 1;
	g_GameFlags |= 5;
	gTS.line = 0;
	gTS.p_write = 0;
	//gTS.wait = 4;
	gTS.flags = 0;

	gTS.rcText.left = TEXT_LEFT;
	gTS.rcText.top = WINDOW_HEIGHT - 56;
	gTS.rcText.right = WINDOW_WIDTH - TEXT_LEFT;
	gTS.rcText.bottom = gTS.rcText.top + LINES * 16;


	// Clear text lines
	for (i = 0; i < LINES + 1; ++i)
	{
		gTS.ypos_line[i] = i * 16;
		CortBox2(&gRect_line, 0x000000, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + i));
		memset(text[i], 0, sizeof(text[0]));
	}

	// Find where event starts
	gTS.p_read = 0;
	while (1)
	{
		// Check if we are still in the proper range
		if (gTS.data[gTS.p_read] == '\0')
			return FALSE;

		// Check if we are at an event
		if (gTS.data[gTS.p_read] == '#')
		{
			// Check if this is our event
			int event_no = GetTextScriptNo(++gTS.p_read);

			if (no == event_no)
				break;
			if (no < event_no)
				return FALSE;
		}

		++gTS.p_read;
	}

	// Advance until new-line
	while (gTS.data[gTS.p_read] != '\n')
		++gTS.p_read;
	++gTS.p_read;

	return TRUE;
}

// End event
void StopTextScript(void)
{
	// End TSC and reset flags
	gTS.mode = 0;
	g_GameFlags &= ~4;
	g_GameFlags |= 3;
	gTS.flags = 0;
}

// Prepare a new line
void CheckNewLine(void)
{
	if (gTS.ypos_line[gTS.line % (LINES + 1)] == LINES * 16)
	{
		gTS.mode = 3;
		g_GameFlags |= 4;
		CortBox2(&gRect_line, 0, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + (gTS.line % (LINES + 1))));
		memset(text[gTS.line % (LINES + 1)], 0, sizeof(text[0]));
	}
}

// Clear text lines
void ClearTextLine(void)
{
	int i;

	gTS.line = 0;
	gTS.p_write = 0;
	gTS.offsetY = 0;

	for (i = 0; i < LINES + 1; ++i)
	{
		gTS.ypos_line[i] = i * 16;
		CortBox2(&gRect_line, 0x000000, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + i));
		memset(text[i], 0, sizeof(text[0]));
	}
}

// Draw textbox and whatever else
void PutTextScript(void)
{
	int i;

	if (gTS.mode == 0)
		return;

	// Set textbox position
	/*
	if (gTS.flags & 0x20)
	{
		gTS.rcText.top = 32;
		gTS.rcText.bottom = gTS.rcText.top + 48;
	}
	else
	{*/
		gTS.rcText.top = WINDOW_HEIGHT - 75;
		gTS.rcText.bottom = gTS.rcText.top + LINES * 16;
	//}

	// Draw textbox
	RECT rcFrame1 = {0, 0, 228, 11};
	RECT rcFrame2 = {0, 11, 228, 17};
	RECT rcFrame3 = {0, 17, 228, 28};

	PutBitmap3(&grcFull, WINDOW_WIDTH / 2 - 114, gTS.rcText.top - 11, &rcFrame1, SURFACE_ID_TEXT_BOX);
	for (i = 0; i < 9; ++i)
		PutBitmap3(&grcFull, (WINDOW_WIDTH / 2) - 114, (i * 6) + gTS.rcText.top, &rcFrame2, SURFACE_ID_TEXT_BOX);
	PutBitmap3(&grcFull, (WINDOW_WIDTH / 2) - 114, (i * 6) + gTS.rcText.top, &rcFrame3, SURFACE_ID_TEXT_BOX);

	// Draw text
	gTS.rcText.top -= 5;
	for (i = 0; i < LINES + 1; ++i)
		PutBitmap3(&gTS.rcText, TEXT_LEFT, (gTS.offsetY + gTS.ypos_line[i] + gTS.rcText.top), &gRect_line, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + i));
	gTS.rcText.top += 5;
}

// Parse TSC
int TextScriptProc(void)
{
	char c[3];
	char str[72];
	int x, y;

	BOOL bExit;

	switch (gTS.mode)
	{
		case 1: // PARSE
			// Type out (faster if ok or cancel are held)
			/*
			++gTS.wait;

			if (!(g_GameFlags & 2) && gKey & (gKeyOk | gKeyCancel))
				gTS.wait += 4;

			if (gTS.wait < 4)
				break;

			gTS.wait = 0;
			*/

			// Parsing time
			bExit = FALSE;

			while (!bExit)
			{
				if (gTS.data[gTS.p_read] == '<')
				{
					if (IS_COMMAND('E','N','D'))
					{
						gTS.mode = 0;
						g_GameFlags |= 3;
						bExit = TRUE;
					}
					else if (IS_COMMAND('N','O','D'))
					{
						gTS.mode = 2;
						gTS.p_read += 4;
						bExit = TRUE;
					}
					else if (IS_COMMAND('C','L','R'))
					{
						ClearTextLine();
						gTS.p_read += 4;
					}
					else
					{
						char str_0[0x40];
						sprintf(str_0, "Unknown code:<%c%c%c", gTS.data[gTS.p_read + 1], gTS.data[gTS.p_read + 2], gTS.data[gTS.p_read + 3]);
						MessageBoxA(NULL, str_0, "Error", MB_OK);
						return 0;
					}
				}
				else
				{
					if (gTS.data[gTS.p_read] == '\r')
					{
						// Go to new-line
						gTS.p_read += 2;
						gTS.p_write = 0;
						++gTS.line;
						CheckNewLine();
					}
					else if (gTS.flags & 0x10)
					{
						// Break if reaches command, or new-line
						while (gTS.data[x] != '<' && gTS.data[x] != '\r')
						{
							// Skip if SHIFT-JIS
							if (gTS.data[x] & 0x80)
								++x;

							++x;
						}

						// Get text to copy
						y = x - gTS.p_read;
						memcpy(str, &gTS.data[gTS.p_read], y);
						str[y] = '\0';

						// This should have been 'y', not 'x'. This mistake causes the blinking
						// cursor to render offscreen. Even if this were fixed, the broken font
						// spacing (see InitTextObject in Draw.cpp) would likely cause it to
						// still appear in the wrong place anyway.
					//#ifdef FIX_BUGS
					//	gTS.p_write = y;
					//#else
						gTS.p_write = x;
					//#endif

						// Print text
						PutText2(0, 1, str, TEXT_SHADOW, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + (gTS.line % (LINES + 1))));
						PutText2(0, 0, str, TEXT_COLOR, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + (gTS.line % (LINES + 1))));
					#ifdef FIX_BUGS
						strcpy(text[gTS.line % (LINES + 1)], str);
					#else
						sprintf(text[gTS.line % (LINES + 1)], str);	// No point in using an sprintf here, and it makes Clang mad
					#endif

						// Check if should move to next line (prevent a memory overflow, come on guys, this isn't a leftover of pixel trying to make text wrapping)
						gTS.p_read += y;

						if (gTS.p_write >= 35)
							CheckNewLine();

						bExit = TRUE;
					}
					else
					{
						// Get text to print
						c[0] = gTS.data[gTS.p_read];

						if (c[0] & 0x80)
						{
							c[1] = gTS.data[gTS.p_read + 1];
							c[2] = '\0';
						}
						else
						{
							c[1] = '\0';
						}

						// Print text
						PutText2(gTS.p_write * 6, 1, c, TEXT_SHADOW, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + (gTS.line % (LINES + 1))));
						PutText2(gTS.p_write * 6, 0, c, TEXT_COLOR, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + (gTS.line % (LINES + 1))));

						strcat(text[gTS.line % (LINES + 1)], c);

						// Offset read and write positions
						if (c[0] & 0x80)
						{
							gTS.p_read += 2;
							gTS.p_write += 2;
						}
						else
						{
							gTS.p_read += 1;
							gTS.p_write += 1;
						}

						if (gTS.p_write >= 35)
						{
							CheckNewLine();
							gTS.p_write = 0;
							++gTS.line;
							CheckNewLine();
						}

						bExit = TRUE;
					}
				}
			}
			break;

		case 2: // NOD
			if (gKeyTrg & (gKeyOk | gKeyCancel))
				gTS.mode = 1;
			break;
	}

	if (gTS.mode == 0)
		g_GameFlags &= ~4;
	else
		g_GameFlags |= 4;

	return 2;
}

void RestoreTextScript(void)
{
	int i;

	for (i = 0; i < LINES + 1; ++i)
	{
		CortBox2(&gRect_line, 0x000000, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + i));
		PutText2(0, 1, text[i], TEXT_SHADOW, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + i));
		PutText2(0, 0, text[i], TEXT_COLOR, (SurfaceID)(SURFACE_ID_TEXT_LINE1 + i));
	}
}

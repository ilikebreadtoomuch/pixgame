#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "WindowsWrapper.h"

#include "Config.h"
#include "Main.h"

const char* const gConfigName = "Config.dat";
const char* const gProof = "DOUKUTSU20001105";

BOOL LoadConfigData(CONFIGDATA *conf)
{
	// Clear old configuration data
	memset(conf, 0, sizeof(CONFIGDATA));

	// Get path
	char path[MAX_PATH];
	sprintf(path, "%s\\%s", gModulePath, gConfigName);

	// Open file
	FILE *fp = fopen(path, "rb");
	if (fp == NULL)
		return FALSE;

	// Read data
	size_t fread_result = fread(conf, sizeof(CONFIGDATA), 1, fp); // Not the original name

	// Close file
	fclose(fp);

	// Check if version is not correct, and return if it failed
	if (fread_result != 1 || strcmp(conf->proof, gProof))
	{
		memset(conf, 0, sizeof(CONFIGDATA));
		return FALSE;
	}

	return TRUE;
}

void DefaultConfigData(CONFIGDATA *conf)
{
	// Clear old configuration data
	memset(conf, 0, sizeof(CONFIGDATA));

	conf->display_mode = 1;
}

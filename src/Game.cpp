#include "Game.h"

#include <stddef.h>
#include <stdio.h>

#include "WindowsWrapper.h"

#include "CommonDefines.h"
#include "Draw.h"
#include "Frame.h"
#include "Generic.h"
#include "GenericLoad.h"
#include "KeyControl.h"
#include "Main.h"
#include "Map.h"
#include "MyChar.h"
#include "Stage.h"
#include "TextScr.h"

int g_GameFlags;
int gCounter;

int Random(int min, int max)
{
	const int range = max - min + 1;
	return (rand() % range) + min;
}

BOOL Game(HWND hWnd)
{
	int frame_x;
	int frame_y;

	unsigned int swPlay;
	unsigned long color = GetCortBoxColor(0);

	swPlay = 1;

	if (!LoadGenericData())
	{
		MessageBoxA(hWnd, "Couldn't read general purpose files", "Error", MB_OK);
		return FALSE;
	}

	InitTextScript2();
	InitMapData2();

	// Reset stuff
	gCounter = 0;
	grcGame.left = 0;
#if WINDOW_WIDTH != 320 || WINDOW_HEIGHT != 240
	// Non-vanilla: these three lines are widescreen-related
	grcGame.top = 0;
	grcGame.right = WINDOW_WIDTH;
	grcGame.bottom = WINDOW_HEIGHT;
#endif
	g_GameFlags = 3;

	gCounter = 0;

	InitMyChar();
	if (!TransferStage(1, 1, 24, 4))
	{
		MessageBoxA(hWnd, "Failed to load stage", "Error", MB_OK);
		return FALSE;
	}
	SetFrameMyChar();
	SetFrameTargetMyChar(18);

	while (1)
	{
		// Get pressed keys
		GetTrg();

		if (gKey & KEY_ESCAPE)
			break;

		if (swPlay % 2 && g_GameFlags & 1)	// The "swPlay % 2" part is always true
		{
			ActMyChar();
			MoveFrame3();
		}

		CortBox(&grcFull, color);
		GetFramePosition(&frame_x, &frame_y);
		PutStage_Back(frame_x, frame_y);
		PutStage_Front(frame_x, frame_y);

		if (swPlay % 2)	// This is always true
		{
			switch (TextScriptProc())
			{
				case 0:
					return 0;
			}
		}

		PutBitmap3(&grcGame, ((gMC.x - gMC.view.front) / 0x200) - (frame_x / 0x200), ((gMC.y - gMC.view.top) / 0x200) - (frame_y / 0x200), &gMC.rect, SURFACE_ID_MY_CHAR);
		PutTextScript();

		if (!Flip_SystemTask(ghWnd))
			break;

		++gCounter;
	}

	EndMapData();

	if (!bFullscreen)
		SaveWindowRect(hWnd, "window.rect");

	return TRUE;
}

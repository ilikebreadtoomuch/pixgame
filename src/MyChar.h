// THIS IS DECOMPILED PROPRIETARY CODE - USE AT YOUR OWN RISK.
//
// The original code belongs to Daisuke "Pixel" Amaya.
//
// Modifications and custom code are under the MIT licence.
// See LICENCE.txt for details.

#pragma once

#include "WindowsWrapper.h"

#include "CommonDefines.h"

typedef struct MYCHAR
{
	unsigned char cond;
	int x;
	int y;
	int tgt_x;
	int tgt_y;
	int index_x;
	int index_y;
	int xm;
	int ym;
	OTHER_RECT view;
	RECT rect;
} MYCHAR;

extern MYCHAR gMC;

void InitMyChar(void);
void ActMyChar(void);
void GetMyCharPosition(int *x, int *y);
void SetMyCharPosition(int x, int y);

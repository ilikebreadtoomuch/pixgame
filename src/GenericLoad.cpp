#include "GenericLoad.h"

#include <stdio.h>

#include "WindowsWrapper.h"

#include "CommonDefines.h"
#include "Draw.h"

BOOL LoadGenericData(void)
{
	BOOL bError;

	bError = FALSE;
	if (!MakeSurface_File("TextBox", SURFACE_ID_TEXT_BOX))
		bError = TRUE;
	if (!MakeSurface_File("MyChar", SURFACE_ID_MY_CHAR))
		bError = TRUE;

	if (bError)
		return FALSE;

	MakeSurface_Generic(256, 256, SURFACE_ID_LEVEL_TILESET, FALSE);

	return TRUE;
}

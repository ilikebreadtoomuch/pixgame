#include "Dialog.h"

#include <stdio.h>

#include "WindowsWrapper.h"

#include "Generic.h"

// All of the original names for the functions/variables in this file are unknown

const char* const gVersionString = 
	"version.%d.%d.%d.%d\r\n"
	"2000/11/5\n"
	"Studio Pixel"
	;

// TODO - Inaccurate stack frame
DLGPROC_RET CALLBACK VersionDialog(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	char string_buffer[104];

	int version1;
	int version2;
	int version3;
	int version4;

	(void)lParam;

	switch (Msg)
	{
		case WM_INITDIALOG:
			GetCompileVersion(&version1, &version2, &version3, &version4);
			sprintf(string_buffer, gVersionString, version1, version2, version3, version4);
			SetDlgItemTextA(hWnd, 1011, string_buffer);

			CenteringWindowByParent(hWnd);

			return TRUE;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case 1:
					EndDialog(hWnd, 1);
					break;
			}

			break;
	}

	return FALSE;
}

DLGPROC_RET CALLBACK QuitDialog(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	switch (Msg)
	{
		case WM_INITDIALOG:
			SetDlgItemTextA(hWnd, 1009, (LPCSTR)lParam);
			CenteringWindowByParent(hWnd);
			return TRUE;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case 2:
					EndDialog(hWnd, 2);
					break;

				case 1:
					EndDialog(hWnd, 1);
					break;
			}

			break;
	}

	return FALSE;
}

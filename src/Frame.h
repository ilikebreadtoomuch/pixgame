#pragma once

typedef struct FRAME
{
	int x;
	int y;
	int *tgt_x;
	int *tgt_y;
	int wait;
} FRAME;

extern FRAME gFrame;

void MoveFrame3(void);
void GetFramePosition(int *fx, int *fy);
void SetFramePosition(int fx, int fy);
void SetFrameMyChar(void);
void SetFrameTargetMyChar(int wait);
